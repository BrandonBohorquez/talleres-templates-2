package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import estructuras.MapNode;
import mundo.FlightMap;

public class FlightRouting {

	public static void main(String[] args) throws Exception{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		boolean seguir = true;
		FlightMap mapa = new FlightMap();
		while (seguir){
			System.out.println("Bienvenido al sistema de mapeo de los vuelos del mundo");
			System.out.println("Seleccione una ocpión: ");
			System.out.println("1. Mostrar la información de un Aeropuerto por ID");
			System.out.println("2. Mostrar la información de un Aeropuerto por código IATA (BOG, IHS, JFK)");
			System.out.println("3. Mostrar los códigos IATA de los aeropuertos");
			System.out.println("4. (BONO) Encontrar vuelo entre dos aeropuertos");

			try{
				String input = br.readLine();

				int opt = Integer.parseInt(input);
				if (opt>4 || opt <1){
					System.out.println("La opción ingresada no existe, pruebe con otra");
				}
				
				switch (opt) {
				case 1:
					//TODO Maneje el caso 1 dada la opción del menú.
					break;
				case 2:
					//TODO Maneje el caso 2 dada la opción del menú.
					break;
				case 3:
					//TODO Maneje el caso 3 dada la opción del menú.
					break;
				default:
					System.out.println("Opción no reconocida, intente de nuevo.");;
				}
			}catch (NumberFormatException num){
				System.out.println("Opción no númerica, ingrese un número");
			}
		}

	}

}
